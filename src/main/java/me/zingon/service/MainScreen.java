package me.zingon.service;

import java.io.IOException;

import freemarker.template.TemplateException;

public interface MainScreen {
	void createMainScreens() throws IOException, TemplateException;

    void createMainScreen(String table) throws IOException, TemplateException;

}

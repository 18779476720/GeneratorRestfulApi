package me.zingon.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import freemarker.template.TemplateException;
import me.zingon.backup.util.Maps;
import me.zingon.backup.util.MyUtil;
import me.zingon.service.CreateService;
import me.zingon.service.MainScreen;

public class MainScreenImpl implements MainScreen {
	CreateService cs=CreateServiceImpl.getInstance();

	@Override
	public void createMainScreens() throws IOException, TemplateException {
		// TODO Auto-generated method stub
		String path= MyUtil.mkFrontDir("screen");
        List<String> tables = Maps.getTables();
        for (String table : tables) {
            Map<String, Object> root = new HashMap<String, Object>();
            root.put("model", MyUtil.a_b2AB(table));
            root.put("fields", Maps.getFields(table));
            cs.write2File(root, "MainScreen.ftl", new File(path + File.separator + MyUtil.a_b2AB(table) + ".screen"));
        }

	}

	@Override
	public void createMainScreen(String table) throws IOException, TemplateException {
		// TODO Auto-generated method stub

	}

}

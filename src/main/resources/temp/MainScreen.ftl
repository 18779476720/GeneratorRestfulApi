<?xml version="1.0" encoding="UTF-8"?>
<!--
    $Author: chenglu
    $Date: ${now?date}.
    $Revision: 1.0  
    $Purpose: 
-->
<a:screen xmlns:a="http://www.aurora-framework.org/application">
	<a:view>
		<a:link id="${model?lower_case}_option_record_query_link" url=""/>
        <a:link id="${model?uncap_first}_create_link" url=""/>
        <a:link id="${model?uncap_first}_edit_link" url=""/>
        <a:link id="${model?lower_case}_query_link" url=""/>
        
        <script><![CDATA[
            function ${model?lower_case}_create() {
                new Aurora.Window({
                    id: '${model}_create_win',
                    url: $('${model?lower_case}_create_link').getUrl(),
                    title: '创建界面',
                    fullScreen: true
                });
            }
            
            function ${model?lower_case}_renderer(value, record, name) {
                var head_id = record.get('head_id');
                var ${model?lower_case}_no = record.get('${model?lower_case}_no');
                var status = record.get('${model?lower_case}_status');
                if (name == '${model?lower_case}_no') {
                    return '<a href="javascript:${model?lower_case}_edit(' + head_id + ',' + '\'' + status + '\'' + ')">' + value + '</a>';
                } else if (name == 'option_record') {
                    return '<a href="javascript:${model?lower_case}_option_record(' + head_id + ')">操作记录</a>';
                }
            }
            
            function ${model?lower_case}_edit(head_id, status) {
                var url = $('${model?lower_case}_query_link').getUrl();
                var titel = '查询界面';
                if (${model?lower_case}_status == 'NEW' || ${model?lower_case}_status == 'REJECTED') {
                    url = $('${model?lower_case}_edit_link').getUrl();
                    titel = '编辑界面';
                }
                new Aurora.Window({
                    id: '${model?lower_case}_detail_win',
                    url: url + '?head_id=' + head_id + '&status=' + status,
                    title: titel,
                    fullScreen: true
                });
            }
            function ${model?lower_case}_option_record(head_id) {
                new Aurora.Window({
                    id: 'operation_record_win',
                    url: $('${model?lower_case}_option_record_query_link').getUrl() + '?operation_table_id=' + head_id + '&operation_table=${model?lower_case}',
                    title: '操作记录',
                    height: 450,
                    width: 600
                });
            }
            
        ]]></script>
        <a:dataSets>
        	<a:dataSet id="${model?lower_case}_query_ds">
        		<a:fields>
        		<#list fields as f>
        			<!--${f.comment}-->
        			<a:field name="${f.field}"/>
        		</#list>
        		</a:fields>
        	</a:dataSet>
        	<a:dataSet id="${model?lower_case}_result_ds" autoCount="true" autoPageSize="true" autoQuery="true" model="" queryDataSet="${model?lower_case}_query_ds" selectable="true"/>
        </a:dataSets>
        <a:screenBody>
        	<a:screenTopToolbar>
                <a:gridButton click="${model?lower_case}_create" text="HN.CREATE"/>
            </a:screenTopToolbar>
            <a:queryForm bindTarget="${model?lower_case}_query_ds" resultTarget="${model?lower_case}_result_ds" style="width:100%;border:none">
	        	<a:formToolBar>
	        		<a:hBox labelWidth="120">
	        			<#list fields as f>
                        	<a:textField name="${f.field}" prompt="${model?upper_case}.${f.field?upper_case}"/>
                        </#list>
                    </a:hBox>
	        	</a:formToolBar>
	        	<a:formBody>
	        	</a:formBody>
        	</a:queryForm>
        	<a:grid id="${model?lower_case}_grid" autoAppend="false" bindTarget="${model?lower_case}_result_ds" marginHeight="180" marginWidth="65" navBar="true">
        		<a:columns>
        			<#list fields as f>
        				<!--${f.comment}-->
        				<a:column name="${f.field}" align="center" prompt="${model?upper_case}.${f.field?upper_case}"/>
        			</#list>
        		</a:columns>
        	</a:grid>
        </a:screenBody>
	</a:view>
</a:screen>
